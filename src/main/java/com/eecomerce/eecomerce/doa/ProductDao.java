package com.eecomerce.eecomerce.doa;

import java.util.List;

import com.eecomerce.eecomerce.model.Product;

public interface ProductDao {
	
	public List<Product> findAll();
	public Product findById(int id);
	public Product save(Product product);

}
