package com.eecomerce.eecomerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EecomerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EecomerceApplication.class, args);
	}

}
