package com.eecomerce.eecomerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eecomerce.eecomerce.doa.ProductDao;
import com.eecomerce.eecomerce.model.Product;

@RestController
public class ProductController {
	
	@Autowired
	private ProductDao productDao;
	
	// Produits
	@GetMapping(value = "Produits")
	public List<Product> listProduits() {
		return productDao.findAll();
	}
	
	//Produits/{id}
	@GetMapping(value = "Produits/{id}")
	public Product aficherUnProduit(@PathVariable int id) {		
		Product product = productDao.findById(id);
		return product;
		
	}
	
	@PostMapping(value = "/Produits")
	public void ajouterProduits(@RequestBody Product product) {
		productDao.save(product);
	}
}
